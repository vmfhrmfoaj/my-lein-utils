(defproject vmfhrmfoaj/my-lein-utils
  "1.0.6-SNAPSHOT"

  :description "A Leiningen utility for helping development."
  :url "https://gitlab.com/vmfhrmfoaj/my-lein-utils"
  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/MIT"}

  :profiles {:dev {:injections [(do
                                  (require '[clojure.data :as data])
                                  (require '[clojure.java.io :as io])
                                  (require '[clojure.java.shell :as shell])
                                  (require '[clojure.pprint :as pp])
                                  (require '[clojure.repl :as repl])
                                  (require '[clojure.string :as str])
                                  (require '[clojure.test :as test])
                                  (require '[clojure.tools.namespace :as ns-tool])
                                  (require '[clojure.tools.namespace.dir   :as ns-dir])
                                  (require '[clojure.tools.namespace.file  :as ns-file])
                                  (require '[clojure.tools.namespace.repl  :as ns-repl])
                                  (require '[clojure.tools.namespace.track :as ns-track])
                                  (require '[expectations :as expect]))]
                   :dependencies [[org.clojure/clojure "1.11.1"]
                                  [org.clojure/tools.namespace "1.2.0"]
                                  [leiningen-core "2.9.5"]]
                   :repl-options {:init-ns dev-helper.plugin}}})
