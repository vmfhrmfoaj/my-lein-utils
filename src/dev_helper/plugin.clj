(ns dev-helper.plugin)


(defn middleware
  [project]
  (let [auto-refresh? (and (:root project)
                           (:auto-refresh project))
        deps (->> (get-in project [:profiles :dependencies])
                  (merge (get project :dependencies))
                  (map first)
                  (into #{}))
        included-ns-tool? (or (contains? deps 'tools.namespace)
                              (contains? deps 'org.clojure/tools.namespace))]
    (cond-> project
      (and (not included-ns-tool?) auto-refresh?)
      (update :dependencies concat [['org.clojure/tools.namespace "1.3.0" :exclude ['org.clojure/clojure]]])

      auto-refresh?
      (update-in
       [:repl-options :init]
       (fn [form]
         (let [dirs (->> [:source-paths :test-paths]
                         (select-keys project)
                         (vals)
                         (apply concat)
                         (vec))
               verbose? (:verbose auto-refresh?)
               rf
               `(binding [*ns* *ns*, *e nil]
                  (apply clojure.tools.namespace.repl/set-refresh-dirs ~dirs)
                  (loop [first?# true
                         tracker# (let [tracker# (apply clojure.tools.namespace.dir/scan
                                                        (clojure.tools.namespace.track/tracker)
                                                        ~dirs)]
                                    (alter-var-root #'clojure.tools.namespace.repl/refresh-tracker (constantly tracker#))
                                    tracker#)]
                    (when clojure.core/*auto-refresh*
                      (let [log# (fn [& xs#] (when ~verbose? (apply println xs#)))
                            notify# (if-let [notify-cmd# (:notify-command ~auto-refresh?)]
                                      (comp (partial apply clojure.java.shell/sh) (partial conj notify-cmd#))
                                      (constantly nil))]
                        (log# tracker#)
                        (try
                          (let [res#
                                (let [repl-state#
                                      (reduce (fn [out# ns#]
                                                (let [ns-state# (some-> ns# (find-ns) (ns-interns))]
                                                  (assoc out# ns#
                                                         (some->> ns-state#
                                                                  (remove #(let [m# (meta (second %))]
                                                                             (and (not (:keep-state m#))
                                                                                  (re-find #"\.clj[cs]?$" (:file m# "")))))
                                                                  (map (juxt first (comp var-get second)))
                                                                  (into {})))))
                                              {} (:clojure.tools.namespace.track/load tracker#))
                                      res# (clojure.tools.namespace.repl/refresh)]
                                  (when-not (= :ok res#)
                                    (clojure.repl/pst *e)
                                    (let [err-ns# (:clojure.tools.namespace.reload/error-ns clojure.tools.namespace.repl/refresh-tracker)
                                          _# (log# "Search for a file for" err-ns# "namespace")
                                          err-file# (clojure.string/replace (str err-ns#) #"\.|-" {"." "/", "-" "_"})
                                          err-file# (->> ~dirs
                                                         (mapcat #(let [base# (str % "/" err-file#)]
                                                                    [(str base# ".clj")
                                                                     (str base# ".cljc")
                                                                     (str base# ".cljs")]))
                                                         (filter #(.exists (clojure.java.io/as-file %)))
                                                         (first))
                                          ns-desc# (when err-file#
                                                     (clojure.tools.namespace.file/read-file-ns-decl err-file#))]
                                      (if-not ns-desc#
                                        (log# "Didn't find a file that occur an error.")
                                        (do
                                          (log# "Create a namespace that was unloaded due to an error.")
                                          (try
                                            (eval ns-desc#)
                                            (catch Throwable _#
                                              (log# "Failed to evaluate `ns` form.")
                                              (when-let [ns# (some-> ns-desc# (second) (symbol))]
                                                (when-not (find-ns ns#)
                                                  (log# "Create a namespace without requires.")
                                                  (binding [*ns* (create-ns ns#)]
                                                    (clojure.core/use 'clojure.core))))))))))

                                  ;; Restores the var defined in REPL.
                                  (doseq [[ns# sym-val#] repl-state#]
                                    (doseq [[sym# val#] sym-val#]
                                      (when-not (find-ns ns#)
                                        (log# "There is no the namespace for" (str "'" ns# "',") "create it.")
                                        (binding [*ns* (create-ns ns#)]
                                          (clojure.core/use 'clojure.core)))
                                      (intern ns# sym# val#)))
                                  res#)]

                            ;; Fire `on-reload` hook.
                            (if-let [sym# (some-> ~auto-refresh? :on-reload (symbol))]
                              (when-let [ns# (some-> sym# (namespace) (symbol))]
                                (when (and (= :ok res#) (not first?#))
                                  (log# "Run" (str "'" sym# "'") "function.")
                                  (try
                                    (require ns#)
                                    (catch Throwable e#
                                      (let [msg# (str "An error was occurred on loading '" sym# "' function")]
                                        (notify# msg#)
                                        (println (str msg# ":")))
                                      (clojure.repl/pst e#)))
                                  (try
                                    (some-> sym# (resolve) (deref) (.invoke tracker#))
                                    (catch Throwable e#
                                      (let [msg# (str "An error was occurred on evaluating '" sym# "' function")]
                                        (notify# msg#)
                                        (println (str msg# ":")))
                                      (clojure.repl/pst e#)))))))
                          (catch Throwable e#
                            (let [msg# (str "Unexpected an error or exception was riased:" e#)]
                              (notify# msg#)
                              (println (str msg# ":"))))))

                      (swap! clojure.core/auto-refresh-counter inc))

                    (recur false
                           ;; NOTE
                           ;;  `refresh-tracker` has been updated from `refresh` function.
                           (let [refresh-time# (:clojure.tools.namespace.dir/time clojure.tools.namespace.repl/refresh-tracker)]
                             (loop [new-tracker# clojure.tools.namespace.repl/refresh-tracker]
                               (if-not (= refresh-time# (:clojure.tools.namespace.dir/time new-tracker#))
                                 new-tracker#
                                 (do
                                   (Thread/sleep 500)
                                   (recur (apply clojure.tools.namespace.dir/scan new-tracker# ~dirs)))))))))]

           `(do
              ;; XXX
              ;;  Is it able to use `intern` instead of complicated code as below?
              (binding [*ns* (find-ns 'clojure.core)]
                (eval '(def ~'auto-refresh-counter (atom 0)))
                (eval '(eval `(def ~(vary-meta '~'*auto-refresh* assoc :dynamic true) true)))
                (eval '(def ~'auto-refresh-thread nil)))
              (require 'clojure.java.io)
              (require 'clojure.java.shell)
              (require 'clojure.repl)
              (require 'clojure.string)
              (require 'clojure.tools.namespace.dir)
              (require 'clojure.tools.namespace.file)
              (require 'clojure.tools.namespace.repl)
              (require 'clojure.tools.namespace.track)

              (.start (Thread.
                       (fn []
                         ;; NOTE
                         ;;  workaround to avoid strange null pointer error at start REPL with Cider
                         (Thread/sleep 1000)
                         (println "Start a refresher thread,"
                                  "watching dirs:"
                                  (->> ~dirs
                                       (interpose ", ")
                                       (apply str)))
                         ;; NOTE
                         ;;  expose the current thread to use `DynamicClassLoader`.
                         (alter-var-root #'clojure.core/auto-refresh-thread (constantly (Thread/currentThread)))
                         (loop []
                           (try
                             (eval ~rf)
                             (catch Throwable e#
                               (println e#)))
                           (recur)))))
              ~form)))))))
